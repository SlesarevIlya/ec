//var d = new Date();
//document.getElementById("year").innerHTML = d.getFullYear();
let _files = [];
let deleted_files = [];
var zip = new JSZip();
var file_counter = 0;
$('#Width').val(getCookie('width'));
$('#Height').val(getCookie('height'));
var masonry = new Masonry('#images', { transitionDuration: 0 });
$(window).on('mouseup', image_mouseup);
selected_holder = null;

auto_height=false;
auto_width=false;

$('#addImage').click(function(){
    $('#file').click();
});


$(document).on('click', '.show', function() {
    var viewer = new Viewer(document.getElementById('images'));
});

class BFile {
    constructor(file) {
        const dot_index = file.name.lastIndexOf('.');
        this.file = file;
        this.size = file.size;
        this.base_name = file.name.substr(0, dot_index);
        this.extension = file.name.substr(dot_index + 1);
        this.fx = this.fy = 0.5;
        this.is_custom_focal= false;
    }
    auto_focal() {
        smartcrop.crop(this.image, {
            width: Math.min(this.width, this.height),
            height: Math.min(this.width, this.height)
        }).then(result => {
            this.fx = result.topCrop.x / this.width;
            this.fy = result.topCrop.y / this.height;
        });
    }
    read(callback) {
        loadImage(this.file, (image, meta) => {
            this.image = image;
            this.width = image.width;
            this.height = image.height;
            this.auto_focal();
            callback(image.toDataURL());
        }, { orientation: true, maxWidth: 400 });
    }

    get filename() {
        let fn = this.base_name + '.';
        if (this.extension.toLowerCase() == 'png' && config.convert_to_jpeg) {
            fn += 'jpg';
        } else {
            fn += this.extension
        }
        return fn;
    }

    get truncated_filename() {
        let filename = this.base_name;
        if (this.base_name.length > 20) {
            filename = this.base_name.substr(0, 15) + '..' + this.base_name.substr(this.base_name.length - 5);
        }
        return filename + '.' + this.extension;
    }

    get is_supported() {
        return ['jpg', 'jpeg', 'png'].indexOf(this.extension.toLowerCase()) > -1
    }

    get focal_x(){
        if(this.is_custom_focal||true){
            return this.fx;
        }else{
            return parseFloat(0.5);
        }
    }

    get focal_y(){
        if(this.is_custom_focal||true){
            return this.fy;
        }else{
            return parseFloat(0.5);
        }
    }
}

$('#continue').click(async function(){
    $("#continueBtn").hide();
    $("#dropzone").toggle();
    $("#wrapper").hide();

    for (let i = 0; i < _files.length; i++) {
        await addImage(_files[i], i);
    }

    async function addImage(file, i) {
        await file.read(content => {
            $('#images').append(`<div id="div_${i}" class="tile">
     <a href="#" class="show">
    <div class="image-holder">
     <img id="file_${i}" class="" src="${content}" >
        <div class="mask mask-1"></div>
        <div class="mask mask-2"></div>
        <div class="mask mask-3"></div>
        <div class="mask mask-4"></div>
        <div class="img-border"></div>
    </div></a>
    <div class="row">
        <div class="col-2" style="margin-top: -13px">
            <div class="cntr">
                <label for="cbx_${i}" class="label-cbx">
                    <input id="cbx_${i}" type="checkbox" class="invisible">
                    <div class="checkbox">
                        <svg width="20px" height="20px" viewBox="0 0 20 20">
                            <path d="M3,1 L17,1 L17,1 C18.1045695,1 19,1.8954305 19,3 L19,17 L19,17 C19,18.1045695 18.1045695,19 17,19 L3,19 L3,19 C1.8954305,19 1,18.1045695 1,17 L1,3 L1,3 C1,1.8954305 1.8954305,1 3,1 Z"></path>
                            <polyline points="4 11 8 15 16 6"></polyline>
                        </svg>
                    </div>
                </label>
            </div>
        </div>
        <div class="col-10">
            <input id="name_${i}" disabled="true" value="${_files[i].truncated_filename}" type="text" class="form-control specialInput" style="border:none;padding-left: 3px;margin-left: 0"> </div>
            </div>
    </div>`);
            masonry.appended(document.querySelector('.tile:last-child'));
            let holder = $('.tile:last-child .image-holder');
            holder.data('file',_files[i]);
            holder.on('mousedown', image_mousedown);
            setTimeout(() => update_preview_all(), 100);
        });
    }

    $("#ImageGrid").toggle();
    count();
});

//select all and clear all
$('#selectAll').click(function(){
    $('.invisible').prop('checked', true);
    count();
});
$('#clearAll').click(function(){
    $('.invisible').prop('checked', false);
    count();
});

$('#autoWidth').change(function() {
    if($("#Width").prop('disabled'))
    {
        auto_width = false;
        $("#Width").prop('disabled', false);
    }
    else
    {
        auto_width = true;
        $("#Width").prop('disabled', true);
    }

});

$('#autoHeight').change(function() {
    if($("#Height").prop('disabled'))
    {
        auto_height = false;
        $("#Height").prop('disabled', false);
    }
    else
    {
        auto_height = true;
        $("#Height").prop('disabled', true);
    }

});

function count() {
    let k = 0;
    var l = _files.length - deleted_files.length;
    $('.invisible').each(function(){
        if($(this).prop('checked')){
            k++;
        }
    });
    $("#total").text(l);
    $("#checked").text(k);

}

function image_mousedown(event) {

    let holder = $(event.originalEvent.target);
    if (!holder.hasClass('image-holder')) {
        holder = holder.closest('.image-holder');
    }
    let file=holder.data('file');
    holder.data('x', event.clientX);
    holder.data('y', event.clientY);

    holder.data('fx', file.focal_x);
    holder.data('fy', file.focal_y);
    selected_holder = holder;
    $(document).off('mousemove');
    $(document).on('mousemove', image_mousemove);

}

function image_mouseup(event) {
    $(document).off('mousemove');
}

function image_mousemove(event) {
    let holder = selected_holder;
    if (!holder.hasClass('image-holder')) {
        holder = holder.closest('.image-holder');
    }

    let x = event.clientX;
    let y = event.clientY;
    let ox = holder.data('x');
    let oy = holder.data('y');

    let fx = holder.data('fx');
    let fy = holder.data('fy');

    let tiles = document.querySelectorAll(".tile");
    for (let i = 0; i < tiles.length; i++) {
        if (tiles[i].querySelector(".invisible").checked) {
            let holder = $(tiles[i].querySelector(".image-holder"));
            let file = holder.data('file');

            let new_fx = fx + (x - ox) / holder.width() * 2;
            let new_fy = fy + (y - oy) / holder.height() * 2;

            new_fx = Math.max(0, Math.min(1, new_fx));
            new_fy = Math.max(0, Math.min(1, new_fy));

            file.fx = new_fx;
            file.fy = new_fy;
            file.is_custom_focal = true;

            if (new_fx != fx || new_fy != fy) {
                // update_preview_single(holder.get(0), file);
                update_preview_single_custom(holder.get(0), file);
            }
        }
    }
}

function image_mousemove_old(event) {
    let holder = selected_holder;
    let file = holder.data('file');
    if (!holder.hasClass('image-holder')) {
        holder = holder.closest('.image-holder');
    }

    let x = event.clientX;
    let y = event.clientY;
    let ox = holder.data('x');
    let oy = holder.data('y');

    let fx = holder.data('fx');
    let fy = holder.data('fy');

    let new_fx = fx + (x - ox) / holder.width() * 2;
    let new_fy = fy + (y - oy) / holder.height() * 2;

    new_fx = Math.max(0, Math.min(1, new_fx));
    new_fy = Math.max(0, Math.min(1, new_fy));

    file.fx = new_fx;
    file.fy = new_fy;
    file.is_custom_focal = true;

    if (new_fx != fx || new_fy != fy) {
        update_preview_single(holder.get(0), file);
    }
}

function update_preview_all() {
    masonry.layout();

    if (auto_width || auto_height) {
        return;
    }
    let holders = document.querySelectorAll('.image-holder');
    for (let i = 0; i < holders.length; i++) {
        update_preview_single(holders[i], _files[i]);
        // update_preview_single_custom(holders[i], _files[i]);
    }
}

function update_preview_all_checkboxes() {
    masonry.layout();

    if (auto_width || auto_height) {
        return;
    }

    let tiles = document.querySelectorAll(".tile");
    for (let i = 0; i < tiles.length; i++) {
        if (tiles[i].querySelector(".invisible").checked) {
            let holder = tiles[i].querySelector(".image-holder");
            //update_preview_single(holder, _files[i]);
            update_preview_single_custom(holder, _files[i]);
        }
    }
}

function update_preview_single_custom2(holder, file) {

    let tw = $('#Width').val();
    let th = $('#Height').val();

    const fx = file.focal_x;
    const fy = file.focal_y;

    const w = holder.offsetWidth;
    const h = holder.offsetHeight;

    const m1 = holder.querySelector('.mask-1');
    const m2 = holder.querySelector('.mask-2');
    const m3 = holder.querySelector('.mask-3');
    const m4 = holder.querySelector('.mask-4');
    const border = holder.querySelector('.img-border');

    let ratio;
    if (auto_width) {
        ratio = h / th;
    } else if (auto_height) {
        ratio = w / tw;
    } else {
        ratio = Math.min(w / tw, h / th);
    }

    const nw = tw * ratio;
    const nh = th * ratio;

    let mw1 = Math.round((w - nw) * fx);
    //let mh1 = Math.round((h - nh) * fy);
    //let mh1 = h;

    let mw2 = Math.round((w - nw) * (1 - fx));
    //let mh2 = Math.round((h - nh) * (1 - fy));
    //let mh2 = h;

    //let mw3 = w;
    //let mw3 = Math.round((w - nw) * fx);
    let mh3 = Math.round((h - nh) * fy);

    //let mw4 = w;
    //let mw4 = Math.round((w - nw) * (1 - fx));
    let mh4 = Math.round((h - nh) * (1 - fy));

    // let bw = w - mw1 - mw2;
    // let bh = h - mh1 - mh2;
    // let bt = mh1;
    // let bl = mw1;
    // if (Math.abs(nh - h) < 0.01) {
    //     bh = h;
    //     bt = 0;
    //     mh1 = mh2 = h;
    // } else {
    //     bw = w;
    //     bl = 0;
    //     mw1 = mw2 = w;
    // }

    $(m1).css({ width: mw1, height: h });
    $(m2).css({ width: mw2, height: h });
    $(m3).css({ width: w, height: mh3 });
    $(m4).css({ width: w, height: mh4 });

    let border_width = 0;
    if (border_width > 5) {
        border_width = Math.max(2, Math.round(border_width * w / file.width));
    }
    // $(border).css({
    //     width: bw, height: bh, top: bt, left: bl,
    //     border: `solid ${border_width}px #000`
    // });

}


function update_preview_single_custom(holder, file) {
    let ratioW = $('#Width').val() / 100;
    let ratioH = $('#Height').val() / 100;

    const fx = file.focal_x;
    const fy = file.focal_y;

    const w = holder.offsetWidth;
    const h = holder.offsetHeight;

    let m_left = holder.querySelector(".mask-1");
    let mw_left = Math.round(w * (1 - ratioW) * fx);
    $(m_left).css({ width: mw_left, height: h });

    let m_right = holder.querySelector(".mask-2");
    let mw_right = Math.round(w * (1 - ratioW) * (1 - fx));
    $(m_right).css({ width: mw_right, height: h });

    let m_top = holder.querySelector(".mask-3");
    let mh_top = Math.round(h * (1 - ratioH) * fy);
    $(m_top).css({ width: w, height: mh_top });

    let m_bottom = holder.querySelector(".mask-4");
    let mh_bottom = Math.round(h * (1 - ratioH) * (1 - fy));
    $(m_bottom).css({ width: w, height: mh_bottom });

    let bw = w - mw_left - mw_right;
    let bh = h - mh_top - mh_bottom;

    const border = holder.querySelector('.img-border');
    let border_width = 2;
    $(border).css({
        width: bw, height: bh, top: mh_top, left: mw_left,
        border: `solid ${border_width}px #000`
    });
}

function update_preview_single(holder, file) {

    let tw = $('#Width').val();
    let th = $('#Height').val();

    const fx = file.focal_x;
    const fy = file.focal_y;

    const w = holder.offsetWidth;
    const h = holder.offsetHeight;

    const m1 = holder.querySelector('.mask-1');
    const m2 = holder.querySelector('.mask-2');
    const border = holder.querySelector('.img-border');

    let ratio;
    if (auto_width) {
        ratio = h / th;
    } else if (auto_height) {
        ratio = w / tw;
    } else {
        ratio = Math.min(w / tw, h / th);
    }

    const nw = tw * ratio;
    const nh = th * ratio;

    let mw1 = Math.round((w - nw) * fx);
    let mh1 = Math.round((h - nh) * fy);

    let mw2 = Math.round((w - nw) * (1 - fx));
    let mh2 = Math.round((h - nh) * (1 - fy));

    let bw = w - mw1 - mw2;
    let bh = h - mh1 - mh2;
    let bt = mh1;
    let bl = mw1;
    if (Math.abs(nh - h) < 0.01) {
        bh = h;
        bt = 0;
        mh1 = mh2 = h;
    } else {
        bw = w;
        bl = 0;
        mw1 = mw2 = w;
    }
    $(m1).css({ width: mw1, height: mh1 });
    $(m2).css({ width: mw2, height: mh2, top: h - mh2 });
    let border_width = 0;
    if (border_width > 5) {
        border_width = Math.max(2, Math.round(border_width * w / file.width));
    }
    $(border).css({
        width: bw, height: bh, top: bt, left: bl,
        border: `solid ${border_width}px #000`
    });

}

function update(ele) {
    if (ele.value === "on") {
        update_preview_all();
    } else {
        update_preview_all_checkboxes();
    }

    //update_preview_all();
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "100";
}

//delete selected image
$('#delete').click(function(){

    $('.invisible').each(function(){
        // use $(this) to reference the current div in the loop
        if($(this).prop('checked')){
            var id = $(this).prop('id');
            id = id.substring(id.indexOf("_") + 1);
            $('#div_'+id).remove();
            deleted_files.push(id);
        }
    });
    count();
});


/**
 Initialize drag and drop
 */
let drop_area = document.getElementById('dropzone');
drop_area.addEventListener('drop', e => {
    e.stopPropagation();
    e.preventDefault();
    this.add_files(e);
    $("#dropzone").removeClass("dropping");
});
drop_area.addEventListener('dragover', e => {
    e.stopPropagation();
    e.preventDefault();
});
drop_area.addEventListener('dragenter', e => {
    e.stopPropagation();
    e.preventDefault();
    $("#dropzone").addClass("dropping");
});

function add_files(e) {
    $("#wrapper").show();
    if (e.target.files) {
        let temp = [];
        temp = e.target.files;
        for (let i = 0; i < temp.length; i++)
        {
            let f = new BFile(temp[i]);

            if (f.is_supported) {
                _files.push(f);
                f.read(content => {
                    $('#preview').append('<div class="td"><div class="box"><img class="img-fluid img-responsive img-thumbnail" src="'+content+'" style="max-height: 150px"> </div></div>');
                })}

        }
    } else if (e['dataTransfer']) {
        let temp = [];
        temp = e.dataTransfer.files;
        for (let i = 0; i < temp.length; i++)
        {
            let f = new BFile(temp[i]);

            if (f.is_supported) {
                _files.push(f);
                f.read(content => {
                    $('#preview').append('<div class="td"><div class="box"><img class="img-fluid img-responsive img-thumbnail" src="'+content+'" style="max-height: 150px"> </div></div>');
                })}
        }
    }
    let count = 0;
    for (let i = 0; i < _files.length; i++) {
        count++;
    }
    if (count>0) {
        _files = _files.sort(function(a,b) {
            return (a.base_name > b.base_name) ? 1
                : ((b.base_name > a.base_name) ? -1
                    : 0);
        });
        $("#continueBtn").show();
    }
    $("#Imagenumber").text(count);
}


//resize functionality
$('#resize').click(function(){
    $('#renameDiv').hide();
    $('#resizeDiv').toggle();
    $(".specialInput").prop('disabled', true);
});

function resize(e) {

    if(!$('#Width').val() && !$('#Height').val())
    {
        alert("Both height and width can't be left empty !!")
    }
    else
    {
        var o = 0;

        $('.invisible').each(function(){
            if(!$(this).prop('checked')){
                o++;
            }
        });

        file_counter = _files.length - deleted_files.length - o;
        if (file_counter > 0) {
            $("#ImageGrid").toggle();
            $("#loading").toggle();

            if ($('#dimesion').prop('checked'))
            {
                let a = _files;
                document.cookie = "height="+$('#Height').val();
                document.cookie = "width="+$('#Width').val();
                // document.cookie = "height="+_files[0].width;
                // document.cookie = "width="+$('#Width').val();
            }

            let a = _files;
            $(".operationCount").text(file_counter);
            $("#operation").text('resized');
            for (let i = 0; i < _files.length; i++)
            {
                var z = 0;
                for(let j = 0;j < deleted_files.length; j++)
                {
                    if(deleted_files[j]==i)
                    {
                        z++;
                    }
                }
                //filtering out deleted and unchecked elements
                if(z==0 && $('#cbx_'+i).prop('checked'))
                {
                    let f = _files[i];
                    loadImage(f.file, (img) => this.process_image(img,f));
                }
            }
        }
        else
        {
            alert("Select at least one image to proceed.")
        }
    }


}

function process_image_new(img,file) {
    var extension = file.extension;
    let tw = $('#Width').val() / 100 * img.width;
    let th = $('#Height').val() / 100 * img.height;

    if(!tw && !th)
    {
        tw = img.width;
        th = img.height;
    }

    const fx = file.focal_x;
    const fy = file.focal_y;

    const iw = img.width;
    const ih = img.height;

    if ($('#autoWidth').prop('checked')) {
        tw = iw * th / ih;
    } else if ($('#autoHeight').prop('checked')) {
        th = ih * tw / iw;
    }

    let canvas = document.createElement('canvas');
    canvas.width = tw;
    canvas.height = th;
    let con = canvas.getContext('2d');
    let scale = Math.min(iw / tw, ih / th);
    let srcw = tw * scale;
    let srch = th * scale;

    let format = 'image/jpeg';
    if (extension == 'png')
    {
        format = 'image/png';
        con.fillStyle = 'white';
        con.fillRect(0, 0, tw, th);
    }
    let hw = 0;

    con.drawImage(img, (iw - srcw) * fx, (ih - srch) * fy, srcw, srch, hw, hw, tw - hw * 2, th - hw * 2);
    canvas.toBlob(b => this.save_zip(b, file.filename),format, 0.8);
}

function process_image(img,file) {
    var extension = file.extension;
    let tw = $('#Width').val();
    let th = $('#Height').val();

    if(!tw && !th)
    {
        tw = img.width;
        th = img.height;
    }

    const fx = file.focal_x;
    const fy = file.focal_y;

    console.log(fx+"|"+fy);

    const iw = img.width;
    const ih = img.height;

    if ($('#autoWidth').prop('checked')) {
        tw = iw * th / ih;
    } else if ($('#autoHeight').prop('checked')) {
        th = ih * tw / iw;
    }

    let canvas = document.createElement('canvas');
    canvas.width = tw;
    canvas.height = th;
    let con = canvas.getContext('2d');
    let scale = Math.min(iw / tw, ih / th);
    let srcw = tw * scale;
    let srch = th * scale;

    let format = 'image/jpeg';
    if (extension == 'png')
    {
        format = 'image/png';
        con.fillStyle = 'white';
        con.fillRect(0, 0, tw, th);
    }
    let hw = 0;

    let drawObj = {
        sx: (iw - srcw) * fx,
        sy: (ih - srch) * fy,
        sWidth: srcw,
        sHeight: srch,
        dx: hw,
        dy: hw,
        dWidth: tw - hw * 2,
        dHeight: th - hw * 2
    };
    con.drawImage(img, (iw - srcw) * fx, (ih - srch) * fy, srcw, srch, hw, hw, tw - hw * 2, th - hw * 2);
    canvas.toBlob(b => this.save_zip(b, file.filename),format, 0.8);
}

function process_image1(img,file,name) {
    var extension = file.extension;
    let tw = $('#Width').val();
    let th = $('#Height').val();

    if(!tw && !th)
    {
        tw = img.width;
        th = img.height;
    }

    const fx = file.focal_x;
    const fy = file.focal_y;

    console.log(fx+"|"+fy);

    const iw = img.width;
    const ih = img.height;

    if ($('#autoWidth').prop('checked')) {
        tw = iw * th / ih;
    } else if ($('#autoHeight').prop('checked')) {
        th = ih * tw / iw;
    }

    let canvas = document.createElement('canvas');
    canvas.width = tw;
    canvas.height = th;
    let con = canvas.getContext('2d');
    let scale = Math.min(iw / tw, ih / th);
    let srcw = tw * scale;
    let srch = th * scale;

    let format = 'image/jpeg';
    if (extension == 'png')
    {
        format = 'image/png';
        con.fillStyle = 'white';
        con.fillRect(0, 0, tw, th);
    }
    let hw = 0;

    con.drawImage(img, (iw - srcw) * fx, (ih - srch) * fy, srcw, srch, hw, hw, tw - hw * 2, th - hw * 2);
    canvas.toBlob(b => this.save_zip(b, name),format, 0.8);
}

function save_zip(b, filename) {
    zip.file(filename, b, {base64: true});
    file_counter--;
    if (file_counter == 0) {
        $("#loading").toggle();
        $("#ImageGrid").toggle();
        $("#message").show().delay(5000).fadeOut();
        $('#resizeDiv').hide();
        $("#resize").hide();
        $('#renameDiv').hide();
        $("#download").show();
    }
}

$(document).on('change', '[type=checkbox]', function() {
    count();
});

//download functionality
$('#download').click(function() {
    zip.generateAsync({type: "blob"}).then(content => saveAs(content, "images.zip"));
});

//rename functionality
$('#rename').click(function() {
    $('#resizeDiv').hide();
    $('#renameDiv').toggle();

    $(".specialInput").prop('disabled', false);
});

var hole = "";
var tray = "";

$('#hole').on('input', function() {
    hole = $(this).val();
    $('.specialInput').each(function(){
        var id = $(this).prop('id');
        id = id.substring(id.indexOf("_") + 1);

        if($('#cbx_'+id).prop('checked'))
        {
            $(this).val(hole+tray);
        }
    });
});

$('#tray').on('input',function(){
    tray = $(this).val();
    if(tray.length>1)
    {
        //incrementing tray
        var num = parseInt(tray.match(/\d+$/));
        var pos = tray.indexOf(num);
        var Str = tray.slice(0,pos);
        $('.specialInput').each(function(){

            tray = Str + num;
            var id = $(this).prop('id');
            id = id.substring(id.indexOf("_") + 1);

            if($('#cbx_'+id).prop('checked'))
            {
                $(this).val(hole+tray);
                num++;
            }
        });
    }
    else
    {
        $('.specialInput').each(function(){
            var id = $(this).prop('id');
            id = id.substring(id.indexOf("_") + 1);

            if($('#cbx_'+id).prop('checked'))
            {
                $(this).val(hole);
            }
        });
    }


});

function rename(e) {
    if(!$('#hole').val() && !$('#tray').val())
    {
        alert("Both hole and tray field can't be left empty !!")

    }
    else
    {
        var o = 0;
        $('.invisible').each(function(){
            if(!$(this).prop('checked')){
                o++;
            }
        });
        file_counter = (_files.length-deleted_files.length-o);

        if(file_counter>0)
        {
            $("#ImageGrid").toggle();
            $("#loading").toggle();

            zip = new JSZip();

            $(".operationCount").text(file_counter);
            $("#operation").text('renamed');
            for (let i = 0; i < _files.length; i++)
            {
                var z = 0;
                for(let j = 0;j < deleted_files.length; j++)
                {
                    if(deleted_files[j]==i)
                    {
                        z++;
                    }
                }
                //filtering out deleted and unchecked elements
                if(z==0 && $('#cbx_'+i).prop('checked'))
                {
                    let f = _files[i];
                    var extension = f.extension;
                    var name = $('#name_'+i).val()+'.'+extension;
                    loadImage(f.file, (img) => this.process_image1(img,f,name));
                }
            }
            $("#rename").hide();
        }
        else
        {
            alert("Select at least one image to proceed.")
        }

    }
}

